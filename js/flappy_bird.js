var graphicsSystem = require('./systems/graphics');
var physicsSystem = require('./systems/physics');
var inputSystem = require('./systems/input');

var bird = require('./entities/bird');
var pipe = require('./entities/pipe');

var FlappyBird = function() {
  this.entities = [new bird.Bird(), new pipe.Pipe(0.15, 0.4, true)];
  this.graphics = new graphicsSystem.GraphicsSystem(this.entities);
  this.physics = new physicsSystem.PhysicsSystem(this.entities);
  this.input = new inputSystem.InputSystem(this.entities);
};

FlappyBird.prototype.run = function() {
  this.graphics.run();
  this.physics.run();
  this.input.run();
};

// Trying to add spacebar input
// window.onkeydown = function(event) {
//   if(event.keyCode === 32) {
//     event.preventDefault();
//     document.getElementById("main-canvas").addEventListener('click', this.event, false);
//   }
// };

function startup() {
  var el = document.getElementsByTagName("canvas")[0];
  el.addEventListener("touchstart", handleStart, false);
}

var ongoingTouches = new Array();

function handleStart(event) {
  event.preventDefault();

  var touches = event.changedTouches;

  for (var i = 0; i < touches.length; i++) {
    ongoingTouches.push(copyTouch(touches[i]));

  }
}

function onTouch(event) {
  event.preventDefault();

  var newEvent = document.createEvent("MouseEvents");
  var type = null;
  var touch = null;

  switch (event.type) {
    case "touchstart":
      type = "mousedown";
      touch = event.changedTouches[0];
      break;
  }

  newEvent.initMouseEvent(type, true, true, event.originalTarget.ownerDocument.defaultView, 0,
    touch.screenX, touch.screenY, touch.clientX, touch.clientY,
    event.ctrlKey, event.altKey, event.shiftKey, event.metaKey, 0, null);
  event.originalTarget.dispatchEvent(newEvent);
}

exports.FlappyBird = FlappyBird;
