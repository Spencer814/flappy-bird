var BirdGraphicsComponent = function(entity) {
  this.entity = entity;
};

BirdGraphicsComponent.prototype.draw = function(context) {
  var position = this.entity.components.physics.position;

  context.fillStyle = '#6495ed';
  context.save();
  context.translate(position.x, position.y);
  context.beginPath();
  context.arc(0, 0, 0.02, 0, 2 * Math.PI);
  context.fill();
  context.lineWidth = 0.001;
  context.strokeStyle = '#000';
  context.stroke();
  context.closePath();
  context.restore();
};

exports.BirdGraphicsComponent = BirdGraphicsComponent;
