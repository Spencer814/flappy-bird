var PipeGraphicsComponent = function(entity) {
  this.entity = entity;
};

PipeGraphicsComponent.prototype.draw = function(context) {
  var position = this.entity.components.physics.position;

  context.fillStyle = '#3c9718';
  context.save();
  context.translate(position.x, position.y);
  context.beginPath();
  context.rect(0, 0, this.entity.size.width, this.entity.size.height - 0.07);
  context.lineWidth = 0.01;
  context.strokeStyle = '#11670d';
  context.stroke();
  context.fill();
  context.restore();

  context.fillStyle = '#3c9718';
  context.save();
  context.translate(position.x, position.y);
  context.beginPath();
  context.rect(0, 0.5, this.entity.size.width, this.entity.size.height + 0.1);
  context.lineWidth = 0.01;
  context.strokeStyle = '#11670d';
  context.stroke();
  context.fill();
  context.restore();
};

exports.PipeGraphicsComponent = PipeGraphicsComponent;
