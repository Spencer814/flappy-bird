var graphicsComponent = require("../components/graphics/pipe");
var physicsComponent = require("../components/physics/physics");

var Pipe = function(width, height, top) {
  this.size = {
    width: width,
    height: height
  };

  console.log("Creating Pipe entity");

  var physics = new physicsComponent.PhysicsComponent(this);
  // var x = 1;
  // var y = 0;
  physics.position.x = 1.5;
  physics.acceleration.x = -0.09;

  if (top) {
    y = 1 - this.size.height;
  }

  var graphics = new graphicsComponent.PipeGraphicsComponent(this);

  this.components = {
    physics: physics,
    graphics: graphics,
  };
};

exports.Pipe = Pipe;
